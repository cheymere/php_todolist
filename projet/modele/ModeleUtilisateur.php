<?php

class ModeleUtilisateur
{
    public static function connexion(string $login, string $mp){
        global $con;

        $ug =new UtilisateurGateway($con);
        try {
            $loginFromDataBase = $ug->getLogin($login);
            $passwdFromDataBase = $ug->getPassword($login);
            if(password_verify($mp, $passwdFromDataBase)){
                $_SESSION['login'] = $login;
                return true;

            }
            else{
                self::deconnexion();
                return false;

            }
        }
        catch (Exception $e){
            return false;
        }



    }

    public static function deconnexion()
    {

        session_destroy();
        //vide les cookies
        setcookie($_COOKIE[session_name()]);
        unset($_COOKIE[session_name()]);
    }
    //fonction pour créer un nouvel utilisateur
    public static function ajoutUtilisateur(string $log, string $mdp, string $email){
        global $con;
        $ug =new UtilisateurGateway($con);
        $ug->createUtilisateur($log, $mdp, $email);
    }

    public static function isUtilisateur() : bool {
        if(isset($_SESSION['login'])) {
            return true;
        }
        else {
            return false;
        }
    }
}


