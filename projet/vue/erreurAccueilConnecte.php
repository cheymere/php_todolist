<!doctype html>
<html lang="fr">
<head>
    <!-- Bootstrap core CSS -->
    <link href="vue/css/bootstrap.css" rel="stylesheet"/>
    <link rel="stylesheet" href="vue/css/offcanvas.css"/>

</head>
<body class="bg-light">

<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <a class="navbar-brand mr-auto mr-lg-0" href="#">ToDoList</a>
    <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Accueil<span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item">
                <a  class="nav-link" href="vue/connexion.php">Connexion</a>
            </li>
        </ul>
        <form method="post" action="index.php?action=deconnecter">
            <button class="deco" type="submit">Deconnexion</button>
        </form>

    </div>
</nav>

<main role="main" class="container">
    <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm">
        <img class="mr-3" src="vue/images/accueil.jpg" alt="" width="150" height="130" style="border-radius: 15px">
        <div class="lh-100 titre">
            <style>
                .titre{
                    color: black;
                }
            </style>
            <?php
            echo "Bonjour ".$_SESSION['login'];
            ?>
            <h6 class="mb-0 lh-100">ToDo List </h6>
        </div>
    </div>

    <form method="post" action="index.php?action=ajouterListePublique" class="form-inline my-2 my-lg-0">
        <div>
            <input class="form-control mr-sm-2 inp" type="text" placeholder="Nom de la liste" name="nomlistePublique" type="submit"/>
            <button class="btn btn-outline-success my-2 my-sm-0 btnAjout" type="submit">ajouter liste publique</button>

        </div>
    </form>
    <form method="post" action="index.php?action=ajouterListePrivees" class="form-inline my-2 my-lg-0">
        <input  class="form-control mr-sm-2 inp" type="text" placeholder="Nom de la liste" name="nomliste" type="submit"/>
        <button class="btn btn-outline-success my-2 my-sm-0 btnAjout" type="submit">ajouter liste privée</button>
    </form>
    <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="border-bottom border-gray pb-2 mb-0">Mes listes</h6>
        <h6 class="border-bottom border-gray pb-2 mb-0">Pas de liste</h6>

</body>
</html>
