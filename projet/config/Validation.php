<?php

class Validation
{
    public function nomValide($name) : bool{
        if (preg_match('#^[a-zA-Z-éàèïëäêîâÉÈÀÏËÄÊÂÎ]{3,30}$#', $name))
            return true;
        return false;

    }

    public function prenomValide($prenom) : bool {
        if (preg_match('#^[a-zA-Z-éàèïëäêîâÉÈÀÏËÄÊÂÎ]{3,30}$#', $prenom))
            return true;
            return false;
    }

    public function dateValide($date) : bool
    {
        if (preg_match('#^[0-3][0-9]/[0-1][0-9]/[1-2][0-9][0-9][0-9]$#', $date))
            return true;
        return false;
    }

    public function emailValide($mail) : bool {
        if (preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#', $mail))
            return true;
            return false;

    }
}