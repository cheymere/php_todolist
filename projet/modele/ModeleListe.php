<?php

class ModeleListe
{
    public static function displayListePubliques() {

        global $con, $rep, $vues;
        $l = new ListeGateWay($con);
        return $results = $l->displayListePublique();

    }
    public static function displayTachesByID($id) {

        $liste = self::getListe($id);
        $liste->set_ListeTache(ModeleTache::displayTachesByID($id));
        return $liste;

    }
    public static function getListe($idliste){

        global $con;
        $ListeGateway = new ListeGateWay($con);
        $liste = $ListeGateway->findByIdListe($idliste);
        return $liste;
    }
    public static function displayListePubliquesPrivees($login){

        global $con;
        $l = new ListeGateWay($con);
        return $l->displayListePriveesPubliques($login);

    }
    public static function ajouterListePubliques($nomliste)
    {
        global $con;
        $l = new ListeGateWay($con);
        $l->createListePublique($nomliste);

    }
    public static function ajouterListePrivees($nomliste, $login)
    {

        global $con, $valid;
        $l = new ListeGateWay($con);
        $l->createListePrivees($nomliste,$login);

    }
    public static function supprimerListe()
    {
        global $con;
        $l = new ListeGateWay($con);
        $idliste = $_REQUEST['idliste'];
        $l->supprimerListe($idliste);
        return $l->displayListePriveesPubliques($_SESSION['login']);

    }
    public static function supprimerListePublique($idliste)
    {
        global $con;
        $l = new ListeGateWay($con);
        $l->supprimerListePublique($idliste);

    }

}