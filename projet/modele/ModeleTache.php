<?php

class ModeleTache
{
    public static function displayTachesByID($id) : array {

        global $con;
        $tacheGateway = new TacheGateWay($con);
         return $tacheGateway->findByIdListe($id);
    }

    public static function ajouterTache($idliste,$nomSaisi,$descriptionSaisie)
    {
        global $con;
        $tacheGateway = new TacheGateWay($con);
        $tacheGateway->createTache($nomSaisi, $descriptionSaisie,$idliste);

    }
    public static function supprimerTache($idTache)
    {
        global $con;
        $tacheGateway = new TacheGateWay($con);
        $idliste = $tacheGateway->findIdListeByIdTache($idTache);
        $liste =  ModeleListe::getListe($idliste);
        $tacheGateway->supprimerTache($idTache);
        $liste->set_ListeTache( self::displayTachesByID($idliste));
        return $liste;
    }

    public static function isChecked($ischecked, $idtache){

        global $con;
        $tacheGateway = new TacheGateWay($con);
        $tacheGateway->updateChecked($ischecked,$idtache);

    }
}