<?php

class Tache
{
    protected $idTache;
    protected $idliste;
    protected $nom;
    protected $description;
    protected $terminee;

    function __construct($idTache,$idliste,$nom, $description,$terminee)
    {
        $this->idTache= $idTache;
        $this->idliste = $idliste;
        $this->nom = $nom;
        $this->description = $description;
        $this->terminee = $terminee;
    }
    public function get_nom(){
        return $this->nom;
    }
   public function get_idTache(){
        return $this->idTache;
    }
    public function get_description(){
        return $this->description;
    }

   public function get_idliste() {
        return $this->idliste;
    }
    public function get_terminee() : bool {
        if($this->terminee == 0)
            return false;
        return true;
    }


}

