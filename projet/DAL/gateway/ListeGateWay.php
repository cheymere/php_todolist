<?php


class ListeGateWay
{
    private $con;

    function __construct(Connection $con)
    {
        $this->con = $con;
    }

    function displayAll()
    {
        $listes = array();
        $query = 'SELECT * FROM liste';
        $stmt = $this->con->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll();
        foreach ($results as $row) {
            array_push( $listes,new Liste($row['idListe'], $row['nomListe'], $row['visible']));
        }
        return $listes;
    }

    function displayListePublique()
    {
        $listes = array();
        $query = 'SELECT * FROM liste where visible=:visible';
        $this->con->executeQuery($query, array(':visible' => array(1, PDO::PARAM_STR)));
        $results = $this->con->getResults();
        foreach ($results as $row) {
            array_push( $listes,new Liste($row['idListe'], $row['nomListe'], $row['visible']));
        }
        return $listes;
    }

    function displayListePriveesPubliques($login)
    {
        $listes = array();
        $query = 'SELECT distinct * FROM utilisateurListe,liste 
                    where utilisateurListe.idListe= liste.idListe 
                    and login=:login';
        $this->con->executeQuery($query, array(':login' => array($login, PDO::PARAM_STR)));
        $results = $this->con->getResults();
        foreach ($results as $row) {
            array_push( $listes,new Liste($row['idListe'], $row['nomListe'], $row['visible']));
        }
        $listes = array_merge($listes,self::displayListePublique());
        return $listes;
    }


    //Fonction qui permet de créer une liste dans la base de données.
    function createListePublique($nom)
    {
        $id = $this->findMaxIdListe();
        $id = $id + 1;
        $query = "insert into liste values('$id','$nom',1)";
        $this->con->executeQuery($query, array(':nom' => array(1, PDO::PARAM_STR),
            ':login' => array(2, PDO::PARAM_STR)));

    }

    function createListePrivees($nom,$login)
    {
        $id = $this->findMaxIdListe();
        $id = $id + 1;
        $query = "insert into liste values('$id','$nom',0)";
        $this->con->executeQuery($query, array(':nom' => array(1, PDO::PARAM_STR),
            ':login' => array(2, PDO::PARAM_STR)));
        $query = "insert into utilisateurliste values('$id','$login')";
        $this->con->executeQuery($query, array(':nom' => array(1, PDO::PARAM_STR),
            ':login' => array(2, PDO::PARAM_STR)));


    }

    function findMaxIdListe()
    {
        $query = "select MAX(idListe) from liste";
        $this->con->executeQuery($query, array());
        $results = $this->con->getResults();
        return $results[0]['MAX(idListe)'];
    }

    function findByIdListe($id)
    {
        $query = "select * from liste where idListe =:id";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_STR)));
        $result = $this->con->getResults();
        foreach ($result as $row) {
            return new Liste($row['idListe'], $row['nomListe'], $row['visible']);; // attribut = attribut d’une table

        }
    }

    function supprimerListePublique($id)
    {

        $query = "delete from tache where idListe=:id";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_STR)));
        $query = "delete from liste where idListe=:idliste";
        $this->con->executeQuery($query, array(':idliste' => array($id, PDO::PARAM_STR)));

    }
    function supprimerListe($id)
    {

        $query = "delete from tache where idListe=:id";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_STR)));
        $query = "delete from utilisateurliste where idListe=:idliste";
        $this->con->executeQuery($query, array(':idliste' => array($id, PDO::PARAM_STR)));
        $query = "delete from liste where idListe=:idliste";
        $this->con->executeQuery($query, array(':idliste' => array($id, PDO::PARAM_STR)));


    }
}



