<!doctype html>
<html lang="fr">
<head>
    <!-- Bootstrap core CSS -->
    <link href="vue/css/bootstrap.css" rel="stylesheet"/>
    <link rel="stylesheet" href="vue/css/offcanvas.css"/>

</head>
<body class="bg-light">

<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <a class="navbar-brand mr-auto mr-lg-3" href="#">ToDoList</a>
    <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Accueil<span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item">
                <a  class="nav-link" href="vue/connexion.php">Connexion</a>
            </li>
        </ul>
        <form method="post" action="index.php?action=deconnecter">
            <button class="deco" type="submit">Deconnexion</button>
        </form>

    </div>
</nav>

<main role="main" class="container">
    <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm">
        <img class="mr-3" src="vue/images/accueil.jpg" alt="" width="150" height="130" style="border-radius: 15px">
        <div class="lh-100 titre">
            <style>
                .titre{
                    color: black;
                }
            </style>
            <?php
            echo "Bonjour ".$_SESSION['login'] . " ! ";
            ?>
            <h6 class="mb-0 lh-100">ToDo List </h6>
        </div>
    </div>

    <form method="post" action="index.php?action=ajouterListePublique" class="form-inline my-2 my-lg-0">
        <div>
            <style>
                .btnAjout{
                    height: auto;
                    width: auto;
                    font-max-size: 10px;
                    border-color: #007bff;
                    color: #007bff;
                }
                .inp{
                    width: auto;
                    margin-bottom: 10px;
                }
                 input:checked + label {
                     text-decoration: line-through;

                 }
                html {
                    margin-top: 65px;
                    height: auto;
                    width: auto;
                    align-items: center;
                    justify-content: center;
                    padding: 50px;
                    background-image: url("vue/images/fond.jpg");
                    background-attachment: fixed;
                    background-size: 2485px;

                }
            </style>
            <input class="form-control mr-sm-2 inp" type="text" placeholder="Nom de la liste" name="nomliste" type="submit"/>
            <button class="btn btn-outline-success my-2 my-sm-0 btnAjout" type="submit">ajouter liste publique</button>

        </div>
    </form>
    <form method="post" action="index.php?action=ajouterListePrivees" class="form-inline my-2 my-lg-0">
        <input  class="form-control mr-sm-2 inp" type="text" placeholder="Nom de la liste" name="nomliste" type="submit"/>
        <button class="btn btn-outline-success my-2 my-sm-0 btnAjout" type="submit">ajouter liste privée</button>
    </form>

    <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="border-bottom border-gray pb-2 mb-0">Mes listes</h6>
        <?php

        foreach ($results as $Liste) { //parcours

            ?>


            <div class="media text-muted pt-3">
                <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg"
                     preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 32x32">
                    <title>Placeholder</title>
                    <rect width="100%" height="100%" fill="#007bff"/>
                    <text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text>
                </svg>
                <form method='post' action="index.php?action=AfficherDetailListe">
                    <input type="hidden" name="idListe" value="<?php echo $Liste->get_id();?>">
                    <button type="submit" class="btn btn-outline-primary"><?php echo $Liste->get_nom();
                        echo "<BR>";?></button>
                </form>
                <form method='post' action="index.php?action=supprimerListePrivePublique">
                    <input type="hidden" name="idliste" value="<?php echo $Liste->get_id();?>">
                    <style>
                        .c{
                            background-color: transparent;
                            border-color: transparent;
                        }
                        .img-c{
                            max-width: 30px;
                        }
                    </style>
                    <button class="btn btn-outline-primary c" type="submit"> <img class="img-c" src="vue/images/corbeille.png"/></button>
                </form>

            </div>

        <?php }

        ?>

</body>
</html>
