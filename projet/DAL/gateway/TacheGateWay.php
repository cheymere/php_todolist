<?php

class TacheGateWay
{

    private $con;

    function __construct(Connection $con)
    {
        $this->con = $con;
    }

    function displayAll()
    {
        $taches = [];
        $query = 'SELECT * FROM tache';
        $stmt = $this->con->prepare($query);
        $stmt->execute();
        $results = $stmt->fetchAll();
       foreach($results as $row)
        {
           $taches = new Tache($row['idTache'],$row['idListe'],$row['nomtache'],$row['Description'],$row['terminee']);
        }
       return $taches;

    }

    //Fonction qui permet de créer une tâche dans la base de données.
    function createTache($nom, $description, $idliste)
    {
        $idtache = $this->findMaxIdTache();
        $idtache = $idtache +1;
        $query = "insert into tache values('$idtache','$idliste','$nom','$description',0)";
        $this->con->executeQuery($query, array(':nomtache' => array(1, PDO::PARAM_STR),
            ':Description' => array(2, PDO::PARAM_STR)));
        return $this->displayAll();

    }
    function findMaxIdTache()  {
        $query = "select MAX(idTache) from tache";
        $this->con->executeQuery($query, array());
        $results = $this->con->getResults();
        return $results[0]['MAX(idTache)'];
    }

    function findTacheByIdTache($nom)
    {
        $query = "select Description from tache where idtache =:nom";
        $this->con->executeQuery($query, array(':nom' => array($nom, PDO::PARAM_STR)));
        $result = $this->con->getResults();
        //var_dump($result);
        return $result[0]['Description'];

    }
    function findIdListeByIdTache($nom)
    {
        $query = "select idListe from tache where idtache =:nom";
        $this->con->executeQuery($query, array(':nom' => array($nom, PDO::PARAM_STR)));
        $result = $this->con->getResults();
        //var_dump($result);
        return $result[0]['idListe'];

    }

    function supprimerTache($id)
    {
        $query = "delete from tache where idtache=:id";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_STR)));

    }
    function findByIdListe($idliste) :  array
    {
        $taches = array();
        $query = "select * from tache where idListe = :idliste";
        $this->con->executeQuery($query, array(':idliste' => array($idliste, PDO::PARAM_STR)));
        $results =  $this->con->getResults();
        foreach ($results as $row) {
            array_push( $taches,new Tache($row['idTache'], $row['idListe'], $row['nomtache'], $row['Description'], $row['terminee']));
        }
        return $taches;
    }
    function updateChecked($ischecked, $idtache){
        $query='UPDATE tache SET terminee =:ischecked WHERE idTache=:id';
        $this->con->executeQuery($query, array(':id'=>array($idtache, PDO::PARAM_INT), ':ischecked'=>array($ischecked, PDO::PARAM_INT)));

    }


}

