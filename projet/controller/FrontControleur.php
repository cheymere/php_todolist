<?php

class FrontControleur
{
    public function __construct()
    {
        global $rep, $vues;
        $erreur = array();

        try {
            session_start(); //démarre la session
            $listeActions = array(
                'UserController' => array('deconnecter', 'ajouterListePrivees', 'AfficherListePrivees','supprimerListePrivePublique','ajouterTachePrivee'),
                'VisiteurControler' => array(NULL,'AfficherDetailListe', 'ajouterTache','supprimerliste', 'supprimertache','ajouterListePublique', 'supprimerTache', 'connecter', 'inscrire','checked'));

            $utilisateur = ModeleUtilisateur::isUtilisateur();
            if (isset($_REQUEST["action"])) {
                $action = $_REQUEST["action"];
            }
            else {
                $action = NULL;
            }
            $ctrl = $this->rechAction($listeActions, $action, $utilisateur);

            if (!isset($ctrl)) {
                $erreur[] = "action inconnue";
                require($rep.$vues['erreur']);
                   exit(1);
            }

            new $ctrl;

        } catch (Exception $exception) {
            $erreur[] = "Action ...";
            require($rep.$vues['erreur']);
        }
    }

    public function rechAction($listeActions, $action, $utilisateur)
    {
        global $rep, $vues;

        foreach ($listeActions as $key => $value) {
            if (in_array($action, $value)) {
                if ($key == 'UserController') {
                    if (!$utilisateur) {
                        require($rep.$vues['connexion']);
                    }
                }
                return $key;
            }
        }
        return NULL;
    }
}