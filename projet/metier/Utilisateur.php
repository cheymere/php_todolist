<?php


class Utilisateur
{
    private $login;
    private $mp;
    private $email;
    //private $listeTache;

    public function __construct(string $identifier, string $password, string $mail){
        $this->login = $identifier;
        $this->mp = $password;
        $this->email = $mail;
    }

    function get_login(): string{
        return $this->login;
    }

    function get_mp(): string{
        return $this->mp;
    }

    function get_email(): string{
        return $this->email;
    }
}