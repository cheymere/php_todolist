<?php


class Liste
{
    protected $idliste;
    protected $nomListe;
    protected $visible;
    protected $listeTache= array();

    function __construct( $idliste, $nomListe,$visible)
    {
        $this->idliste = $idliste;
        $this->nomListe = $nomListe;
        $this->visible= $visible;
    }
    function get_nom(){
        return $this->nomListe;
    }
    function set_ListeTache(array $listeTache) : void
    {
        $this->listeTache = $listeTache;
    }

    function get_ListeTache() : array
    {
       return $this->listeTache ;
    }
    function get_id() {
        return $this->idliste;
    }
    function get_visible() {
        return $this->visible;
    }



}
