<?php

class VisiteurControler
{
    //ajouter une liste publique
    function __construct()
    {
        global $rep, $vues;
        $erreur = array();

        try {
            $action = $_REQUEST['action'];
            switch ($action) {
                case NULL:
                    $this->reinit();
                    break;
                case "ajouterTache":
                    $this->ajouterTache();
                    break;
                case "ajouterListePublique":
                    $this->creerListe();
                    break;
                case "supprimertache":
                    $this->supprimertache();
                    break;
                case "supprimerliste":
                    $this->supprimerListe();
                    break;
                case "AfficherDetailListe":
                    $this->detailListe();
                    break;
                case "connecter":
                    $this->ValidationFormulaire();
                    break;
                case "inscrire":
                    $this->inscrireUtilisateur();
                    break;
                    case "checked":
                        $this->updateCheck();
                        break;

                //mauvaise action
                default:
                    $erreur[] = "Erreur d'appel php";
                    require ($rep. $vues['erreur']);
                    break;
            }
        }
        catch (PDOException $e){
            $erreur[] = "Erreur inattendue aaa !!!";
            require ($rep. $vues['erreur']);
        }
        catch (Exception $e2){
            $erreur[] = "Erreur inattendue !!!";
            require ($rep. $vues['erreur']);
        }
        exit(0);
    }

    function reinit() {

        global $rep, $vues;
        if(!isset($_SESSION['login'])){

            $results = ModeleListe::displayListePubliques();
             if(!empty($results)){
                require($rep . $vues['acceuil']);
             }
             else {
                 require($rep . $vues['pasDeListe']);
             }
        }
        else{
            $results = ModeleListe::displayListePubliquesPrivees($_SESSION['login']);
            require($rep . $vues['acceuilConnecte']);
        }


    }
    function creerListe()
    {
        global $rep, $vues,$valid;
        $nomliste = $_POST['nomliste'];
        if ($valid->nomValide($nomliste)) {
            ModeleListe::ajouterListePubliques($nomliste);
                self::reinit();
        }
        else {
                $erreur[] = "Veuillez saisir un nom de liste valide";
                require_once($rep . $vues['erreur']);
        }


    }
    function ajouterTache() {
        global $valid, $rep, $vues, $erreur;
       $idliste = $_REQUEST['idliste'];
        $nomSaisi = $_POST['nomtache'];
        $descriptionSaisie = $_POST['description'];
        if ($valid->nomValide($nomSaisi) && $valid->nomValide($descriptionSaisie)) {
            try {
                ModeleTache::ajouterTache($idliste,$nomSaisi,$descriptionSaisie);
                header('Refresh:0;url=index.php?action=AfficherDetailListe&idListe='. $idliste);
            }
            catch (Exception $e) { //si la tache existe
                $erreur[] = "Erreur ajout de la tache";
                require_once($rep . $vues['erreur']);
            }
        } else {
            $erreur[] = "Veuillez saisir un nom ou une description valide";
            require_once($rep . $vues['erreur']);
        }
    }
    function supprimerListe() {

        global $rep, $vues;
        $idliste = $_REQUEST['idliste'];
        ModeleListe::supprimerListePublique($idliste);
        $results = ModeleListe::displayListePubliques();
        if(!empty($results))
            require($rep . $vues['acceuil']);
        else {
            require($rep . $vues['pasDeListe']);
        }

    }
    function supprimertache() {
        global $rep, $vues;

        $idtache = $_REQUEST['idtache'];
        $liste = ModeleTache::supprimerTache($idtache);
        require_once($rep .$vues['detailListe']);


    }
    function detailListe(){
        global $rep, $vues;

        $idliste = $_REQUEST['idListe'];
        $liste= ModeleListe::displayTachesByID($idliste);
        require_once($rep .$vues['detailListe']);


    }
    function ValidationFormulaire() {
        global $rep, $vues;

        $login=$_POST['inputLogin']; // inputLogin = nom du champ texte dans le formulaire
        $mp=$_POST['inputPassword'];
        //utilisation de ModeleUtilisateur pour la validation de la connexion
       if(ModeleUtilisateur::connexion($login,$mp)) {
           self::reinit();
       }
       else {
           $erreur[] = "Login ou mot de passe incorrect";
           require_once($rep . $vues['erreur']);
       }

    }
    function inscrireUtilisateur(){

        global $rep, $vues, $erreur;
        $login = $_POST['loginSaisi'];
        $mp = $_POST['mpSaisi'];
        $email = $_POST['email'];

        try {
            ModeleUtilisateur::ajoutUtilisateur($login, $mp, $email);
            require_once($rep .$vues['acceuilConnecte']);

        }
        catch (Exception $e){
            $erreur[] = "Erreur inscription, ce login existe déjà ";
            require_once($rep .$vues['erreur']);

        }

    }
    function updateCheck(){
        $ischecked = $_REQUEST['ischecked'];
        $idtache = $_REQUEST['idtache'];
        ModeleTache::isChecked($ischecked, $idtache);
        self::reinit();


    }

}