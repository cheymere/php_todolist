<!doctype html>
<html lang="fr">
<head>
    <link href="vue/css/bootstrap.css" rel="stylesheet"/>
    <link rel="stylesheet" href="vue/css/offcanvas.css"/>
</head>
<body class="bg-grey">
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <a class="navbar-brand mr-auto mr-lg-0" href="#">ToDoList</a>
    <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Accueil<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a  class="nav-link" href="vue/connexion.php">Connexion</a>
            </li>
        </ul>
        <?php
            if(isset($_SESSION['login'])){
        ?>
        <form method="post" action="index.php?action=deconnecter">
            <button class="deco" type="submit">Deconnexion</button>
        </form>
        <?php
            }
        ?>


    </div>
</nav>
</nav>
</body>
<main role="main" class="container">
    <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm">
        <img class="mr-3" src="vue/images/accueil.jpg" alt="" width="48" height="48">
        <div class="lh-100">
            <h6 class=" titre">ToDo List</h6>
        </div>
    </div>

    <h6 class="border-bottom border-gray pb-2 mb-0">Tâches</h6>
    <div class="my-3 p-3 bg-white rounded shadow-sm">
        <form action="index.php?action=ajouterTache" method="post" class="form-inline my-2 my-lg-0">
            <input  class="form-control mr-sm-2" type="text" placeholder="Nom de la tâche" name="nomtache"  type="submit" aria-label="Search">
            <input type="hidden" name="idliste" value="<?php echo $liste->get_id();?>">
            <input  class="form-control mr-sm-2" type="text" placeholder="Description" name="description"  type="submit">

            <button  class="btn btn-outline-success my-2 my-sm-0" type="submit">Ajouter la tâche</button>
        </form>

        <?php

            foreach (($liste->get_ListeTache()) as $tache) { //parcours
                ?>
                    <div class="d-inline-flex m-3">
                    <form method="post" action="index.php?action=checked">
                    <?php
                        if($tache->get_terminee()) {
                            ?>
                            <input type="checkbox" checked><input type="hidden" name="ischecked" value="0" >
                            <del><?php echo $tache->get_nom() . " : ";?></del>
                            <?php echo $tache->get_description() . "<BR>";
                        }
                        else {
                            ?>
                            <input type="checkbox" name="ischecked" value="1">
                            <?php
                                print $tache->get_nom() . " : ";

                                echo $tache->get_description() . "<BR>";
                        }   ?>
                        <input type="hidden" name="idtache" value="<?php echo $tache->get_idTache()?>">
                        <button type="submit" class="btn btn-outline-primary mt-3" >Save</button>
                        </form>

                <form method='post' action="index.php?action=supprimertache">
                    <input type="hidden" name="idtache" value="<?php echo $tache->get_idTache();?>">

                        <input type="hidden" name="idliste" value="<?php echo $tache->get_idliste();?>">
                        <button type="submit" class="btn btn-outline-primary c"><img class="img-c" src="vue/images/corbeille.png"/></button>
                </form>
                    </div>


            <?php }

        ?>


    </div>

