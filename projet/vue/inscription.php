<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Inscription</title>

    <!-- Bootstrap core CSS-->
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/floating-labels.css"/>
    <link rel="stylesheet" href="css/offcanvas.css"/>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

</head>
<body>
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <a class="navbar-brand mr-auto mr-lg-0" href="accueil.php">ToDo List</a>
    <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#">Notifications</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="connexion.php">Connexion</a>
            </li>
        </ul>
        <form action="/../projet/vues/bootstrap/connexion.php?action=deconnecter">
            <button type="submit">Deconnexion</button>
        </form>
    </div>
</nav>



<form class="form-signin" method="POST" action="/../projet/index.php?action=inscrire">
    <div class="text-center mb-4">
        <img class="mb-4" src="/../projet/vues/bootstrap/images/coImg.jpg" alt="" width="150" height="200">
        <h1 class="h3 mb-3 font-weight-normal">Inscription</h1>
        <p>S'inscrire sur la ToDo List</p>
    </div>

    <div class="mb-3">
        <label for="loginSaisi">Login</label>
        <input class="form-control" name="loginSaisi" placeholder="Login" required autofocus>
    </div>

    <div class="mb-3">
        <label for="inputPassword">Password</label>
        <input type="password" name="mpSaisi" class="form-control" placeholder="Password" required>
    </div>

    <div class="mb-3">
        <label for="email">Email <span class="text-muted">(Optional)</span></label>
        <input type="email" class="form-control" name="email" placeholder="you@example.com" required>
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Valider</button>
    <p class="mt-5 mb-3 text-muted text-center">&copy; 2020-2021</p>
</form>

</body>
</html>
