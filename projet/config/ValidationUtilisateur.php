<?php

class Validation
{
    public function loginValide($login){
        if (preg_match('#^[a-zA-Z-0-9]{3,30}$#', $login)) {
            echo "Your login is ok.";
        } else {
            echo "Wrong login";
        }
    }

    public function passwordValide($password){
        if (preg_match('#^[a-zA-Z-0-9]{3,30}$#', $password)) {
            echo "Your password is ok.";
        } else {
            echo "Wrong password";
        }
    }

    public function emailValide($mail){
        if (preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#', $mail)) {
            echo "Your mail is ok.";
        } else {
            echo "Wrong mail.";
        }
    }

}