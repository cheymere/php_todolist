<?php

class UserController
{
    function __construct() {

        global $rep,$vues;
        $erreur = array();
        try {
            $action = $_REQUEST['action'];
            switch ($action) {
                case "ajouterListePrivees":
                    $this->creerListePrivee();
                    break;
                case "supprimerListePrivePublique":
                    $this->supprimerListe();
                    break;
                case "AfficherDetailListe":
                    $this->detailListe();
                    break;
                case "supprimertache":
                    $this->supprimertache();
                    break;
                case "afficherDetailTache":
                    $this->afficherDetailTache();
                    break;
                case 'deconnecter':
                    $this->DeconnexionForm();
                    break;


                //mauvaise action
                default:
                    $erreur[] = "Erreur d'appel php";
                    require ($rep. $vues['erreur']);
                    break;

                /*default:
                $erreur[] = "Erreur d'appel php";
                    require ($rep. $vues['erreur']);
                break;*/

            }
        }
        catch (PDOException $e)
        {
            $erreur[] = "Erreur de connexion ";
            require ($rep. $vues['erreur']);

        }

    }

    function creerListePrivee()
    {
        global $rep, $vues, $valid;
        $nomliste = $_POST['nomliste'];
        $login = $_SESSION['login'];

        if ($valid->nomValide($nomliste)) {

            ModeleListe::ajouterListePrivees($nomliste,$login);
            $results = ModeleListe::displayListePubliquesPrivees($login);
            require($rep . $vues['acceuilConnecte']);

        }
        else {
            $erreur[] = "Veuillez saisir un nom de liste valide";
            require_once($rep . $vues['erreur']);
        }


    }
    function supprimerListe() {

        global $rep, $vues;
        $results = ModeleListe::supprimerListe();
        if(!empty($results))
            require($rep . $vues['acceuilConnecte']);
        else
            require($rep . $vues['pasDeListeConnecte']);
    }
    function supprimertache() {

        $idtache = $_REQUEST['idtache'];
        ModeleTache::supprimerTache($idtache);

    }

    function detailListe(){

        global $rep, $vues;

        $liste = $_REQUEST['idListe'];
        $results = ModeleTache::displayTachesByID($liste);
        if(!empty($results))
            require($rep . $vues['acceuilConnecte']);
        else
            require($rep . $vues['pasDeListePrivee']);

    }
    function afficherDetailTache(){

        $modeletache = new ModeleTache();
        $modeletache->afficherDetailTache();
    }


    function DeconnexionForm(){

        global $rep,$vues;
        $m = new ModeleUtilisateur();
        $m->deconnexion();
        $results = ModeleListe::displayListePubliques();
        require_once($rep .$vues['acceuil']);


    }



}