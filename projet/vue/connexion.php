<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Connexion</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/floating-labels.css"/>
    <link rel="stylesheet" href="css/offcanvas.css"/>

</head>
<body>
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <a class="navbar-brand mr-auto mr-lg-3" href="#">ToDo List</a>
    <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="../index.php">Accueil<span class="sr-only">(current)</span></a>
            </li>
        </ul>

    </div>
</nav>



<form class="form-signin" method="POST" action="../index.php?action=connecter">
    <div class="text-center mb-4">
        <img class="mb-4" src="images/coImg.jpg" alt="" width="150" height="200">
        <h1 class="h3 mb-3 font-weight-normal">Connexion</h1>
        <p>Se connecter à ToDo List</p>
    </div>

    <div class="form-label-group">
        <input name="inputLogin" class="form-control" placeholder="Login" required autofocus >
        <label for="inputLogin">Login</label>
    </div>

    <div class="form-label-group">
        <input type="password" name="inputPassword" class="form-control" placeholder="Password" required>
        <label for="inputPassword">Password</label>
    </div>

    <div>
        <p>Nouveau, viens t'inscrire <a href="inscription.php">ici</a></p>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
    <p class="mt-5 mb-3 text-muted text-center">&copy; 2020-2021</p>
</form>

</body>
</html>