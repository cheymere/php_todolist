<?php

class UtilisateurGateway
{
    private $con;

    public function __construct(Connection $con)
    {
        $this->con = $con;
    }

    public function showUtilisateurs(){
        $query = "select login, email from T_Utilisateur";
        $this->con->executeQuery($query, array(
            ':login'=>array(1, PDO::PARAM_STR),
            ':email'=>array(3, PDO::PARAM_STR)
        ));

        $result = $this->con->getResults();
        foreach ($result as $row){
            Print $row['login'];
            echo " ";

            Print $row['email'];
            echo "<br>";
        }
    }

    public function getLogin(string $login){
        $query = 'select login from T_Utilisateur where login=:login';
        $this->con->executeQuery($query, array(
            ':login'=>array($login, PDO::PARAM_STR)
        ));

        $result = $this->con->getResults();
        return $result;
    }

    public function getPassword($login){
        $query = 'select mp from T_Utilisateur where login=:login';
        $this->con->executeQuery($query, array(
            ':login'=>array($login, PDO::PARAM_STR)
        ));

        $result = $this->con->getResults();
        if(!empty($result)){
            foreach ($result as $row) {
                return $row['mp'];
            }
        }

    }

    //fonction pour créer un nouvel utilisateur
    public function createUtilisateur(string $log, string $mdp, string $mail){
        //Hash du mp
        $pass_hash = password_hash($mdp, PASSWORD_DEFAULT);
        $query = "insert into T_Utilisateur values('$log', '$pass_hash', '$mail')";

        $this->con->executeQuery($query, array(
            ':login'=>array($log, PDO::PARAM_STR),
            ':mp'=>array($pass_hash, PDO::PARAM_STR),
            ':email'=>array($mail, PDO::PARAM_STR)
        ));
    }

    //fonction pour suprimer l'utilisateur dont le login est passé en paramètre
    public function deleteUtilisateur(string $login)
    {
        $query = "delete from T_Utilisateur where login=:login";
        $this->con->executeQuery($query, array(
            ':login' => array($login, PDO::PARAM_STR),
        ));
    }

    public function findUtilisateur(string $login, string $mp)
    {
        $query = "select login, mp from T_Utilisateur where login=:login";

        $this->con->executeQuery($query, array(
            ':login' => array($login, PDO::PARAM_STR)
        ));

        $res = $this->con->getResults();

        foreach ($res as $r) {
            $test = password_verify($mp, $r['mp']);
            if($test == 1){
                print "ok";
            }
            else {
                print "Wrong password";
            }
        }
    }

    public function isUtilisateur($login, $mp){
        if(isset($_SESSION['login']) && isset($_SESSION['role'])) {
            $role = filter_var($_SESSION['role'], FILTER_SANITIZE_STRING);
            $login = filter_var($_SESSION['login'], FILTER_SANITIZE_STRING);
            echo "ok";
        }
        else {
            echo "no";
            return NULL;
        }
    }




}

//utilise la connection de config.php
//$u = new UtilisateurGateway($con);
//$u->getPassword("chloe");
//$u->createUtilisateur('didier','1234','didier@gmail.fr');
//$u->getLogin("chloe");
//$u->findUtilisateur("meriem", "14");
//$u->deleteUtilisateur('didier');
//$u->showUtilisateurs();
//echo '------------------', "<br>";
//$u->findByName('laura');

//$pass = "MotDEPass3";
//echo password_hash($pass, PASSWORD_DEFAULT);

/*
$pass = "234";
$pass_hash = password_hash($pass, PASSWORD_DEFAULT);
$test = password_verify($pass, $pass_hash);
echo $test;
*/




