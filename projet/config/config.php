<?php

require_once('Connection.php');
require_once('Validation.php');

// général pour trouver dossier
$rep = __DIR__ . '/../';


//BD
$user = 'root';
$passwd = "";
$dsn = 'mysql:host=localhost;dbname=todolist';
$con = new Connection($dsn, $user, $passwd);
$valid = new Validation();

//array d'erreur
$erreur = array();
//Vues
$vues['acceuil'] = 'vue/acceuil.php';
$vues['pasDeListe'] = 'vue/erreurAcceuil.php';
$vues['pasDeListeConnecte'] = 'vue/erreurAccueilConnecte.php';
$vues['connexion'] = 'vue/connexion.php';
$vues['detailListe'] = 'vue/detailListe.php';
$vues['acceuilConnecte'] = 'vue/accueilConnecte.php';
$vues['erreur'] = 'vue/erreur.php';
