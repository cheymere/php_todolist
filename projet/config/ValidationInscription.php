<?php

class Validation
{
    public function __construct(){
       session_start();
    }

    static function val_action($action){
        if(!isset($action)){
            throw new Exception("pas d'action");
        }
    }

    static function val_form(string &$login, string &$mp, array &$vueErreurUtilisateur)
    {
        //expression régulière pour le login et le mp
        if (!preg_match('#^[a-zA-Z-0-9]{3,30}$#', $login)) {
            $vueErreurUtilisateur[] = "Wrong login";
            $login = "";
            require(__DIR__ . "/../vues/bootstrap/erreur.php");
        }
        else if(!preg_match('#^[a-zA-Z-0-9]{3,30}$#', $mp)){
            $vueErreurUtilisateur[] = "Wrong password";
            $mp = "";
            require(__DIR__ . "/../vues/bootstrap/erreur.php");
        }
        else {
            require(__DIR__."/../vues/bootstrap/accueil.php");
            //require ("index.php");
        }
    }



    /*
    public function emailValide($mail){
        if (preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#', $mail)) {
            echo "Your mail is ok.";
        } else {
            echo "Wrong mail.";
        }
    }*/
}